﻿using UnityEngine;
using UnityEngine.AI;

public class CharacterController : MonoBehaviour {

    public Transform target;

    private Vector3 startPosition;

    private NavMeshAgent agent;

    private CandyCoded.CameraFollow cameraFollow;
    private GameObject cameraFollowSecondaryTarget;

    void Awake() {

        startPosition = gameObject.transform.position;

        agent = GetComponent<NavMeshAgent>();
        agent.destination = target.position;

        cameraFollow = gameObject.GetComponent<CandyCoded.CameraFollow>();
        cameraFollowSecondaryTarget = new GameObject();

    }

    void Update() {

        cameraFollow.secondaryTarget = cameraFollowSecondaryTarget.transform;
        cameraFollowSecondaryTarget.transform.position = agent.path.corners[1];

    }

    void OnTriggerEnter(Collider other) {

        if (other.isTrigger && other.name.Equals("Target")) {

            gameObject.transform.position = startPosition;

        }

    }

}
